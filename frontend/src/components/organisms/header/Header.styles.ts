import { css } from '@emotion/react'
const styles = {
  wrapper: css`
    padding: 16px;
    display: flex;
    align-items: center;
  `,
  leftContent: css`
    display: flex;
    flex: 1;
    align-items: center;

    p {
      margin-left: 12px;
    }
  `,
  row: css`
    display: flex;
    text-decoration: none;
  `,
}

export default styles
