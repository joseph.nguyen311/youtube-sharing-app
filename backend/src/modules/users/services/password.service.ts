import { pbkdf2Sync, randomBytes } from 'crypto';

export class PasswordService {
  async toHash(password: string) {
    const salt = randomBytes(8).toString('hex');
    const passwordHash = pbkdf2Sync(
      password,
      salt,
      10000,
      64,
      'sha512',
    ).toString('hex');

    return `${passwordHash}.${salt}`;
  }

  async compare(suppliedPassword: string, storedPassword: string) {
    const [hashedPassword, salt] = storedPassword.split('.');
    const buf = pbkdf2Sync(
      suppliedPassword,
      salt,
      10000,
      64,
      'sha512',
    ) as Buffer;

    return buf.toString('hex') === hashedPassword;
  }
}
