import {
  Routes,
  Route,
  matchRoutes,
  useLocation,
  Navigate,
} from 'react-router-dom'

import ShareVideo from 'pages/shareVideo'
import SharedVideoList from 'pages/sharedVideoList'

interface RouteProps {
  path: string
  element: any
  metaData?: IRouteMeta
}

export const ROUTE_PATHS = {
  DEFAULT: '/',
  SHARE_VIDEO: '/share',
}

export interface IRouteMeta {
  noHeader?: boolean
  hideGlobalMenu?: boolean
}

export const ROUTES: RouteProps[] = [
  {
    path: ROUTE_PATHS.DEFAULT,
    element: SharedVideoList,
  },
  {
    path: ROUTE_PATHS.SHARE_VIDEO,
    element: ShareVideo,
  },
]

const Router = () => {
  return (
    <Routes>
      {ROUTES.map((r) => (
        <Route key={r.path} path={r.path} element={<r.element />} />
      ))}

      <Route
        key='other'
        path='*'
        element={<Navigate to={ROUTE_PATHS.DEFAULT} replace />}
      />
    </Routes>
  )
}

export const useRouteMetaData = () => {
  const location = useLocation()
  const arr = matchRoutes(ROUTES, location)

  return arr?.[0].route.metaData
}
export default Router
