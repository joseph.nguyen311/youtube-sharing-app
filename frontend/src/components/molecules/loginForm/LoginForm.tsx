import { Stack } from '@mui/material';
import { FC } from 'react';
import { useForm, Controller, SubmitHandler } from 'react-hook-form';
import { yupResolver } from '@hookform/resolvers/yup';
import * as yup from 'yup';
import { Button, TextField } from 'components/atoms';

import { ILoginRequest } from 'types/auth';

const schema = yup.object({
	username: yup.string().email().required(),
	password: yup.string().required(),
}).required();

type FormData = yup.InferType<typeof schema>;

interface IProps {
	onSubmit: (params: ILoginRequest) => void
}

const LoginForm: FC<IProps> = (props) => {
	const { control, handleSubmit, formState: { errors } } = useForm<FormData>({
		resolver: yupResolver(schema),
	});

	const handleLogin: SubmitHandler<ILoginRequest> = (data: ILoginRequest) => {
		props.onSubmit(data);
	}

	return (
		<div>
			<form onSubmit={handleSubmit(handleLogin)}>
				<Stack direction={{ xs: 'column', sm: 'row' }} spacing={2} >
					<Controller
						name='username'
						control={control}
						render={({ field }) => <TextField error={!!errors?.username?.message} label='Email' {...field} />}
					/>
					<Controller
						name='password'
						control={control}
						render={({ field }) => <TextField error={!!errors?.password?.message} type='password' label='Password' {...field} />}
					/>

					<Button type='submit' variant='contained'>Login / Register</Button>
				</Stack>
			</form>
		</div>
	);
};

export default LoginForm;
