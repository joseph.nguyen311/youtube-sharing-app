const eslintConfig = require('./.eslintrc.js')

module.exports = {
  ...eslintConfig,
  rules: {
    ...eslintConfig.rules,
    'comma-dangle': [2, 'always-multiline'],
    'no-console': [2, { allow: ['warn', 'error', 'debug', 'info'] }],
    'no-debugger': 2,
    'no-unused-vars': 2,
  },
}
