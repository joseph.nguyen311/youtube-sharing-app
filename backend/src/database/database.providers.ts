/* istanbul ignore file */
import { DataSource } from 'typeorm';
import { ConfigService } from '@nestjs/config';
import { DATA_SOURCE } from 'src/common';
import { UserEntity } from 'src/modules/users/entities';
import { LinkEntity } from 'src/modules/links/entities';

export const databaseProviders = [
  {
    provide: DATA_SOURCE,
    useFactory: async (configService: ConfigService) => {
      const dataSource = new DataSource({
        type: 'mysql',
        host: configService.get('VSA_MYSQL_HOST'),
        port: +configService.get('VSA_MYSQL_PORT'),
        username: configService.get('VSA_MYSQL_USER'),
        password: configService.get('VSA_MYSQL_PASSWORD'),
        database: configService.get('VSA_MYSQL_DB'),
        entities: [UserEntity, LinkEntity],
        migrations: [__dirname + '/../migrations/*{.ts,.js}'],
        synchronize: false,
        migrationsRun: false,
        logging: configService.get('NODE_ENV') === 'local',
      });

      return dataSource.initialize();
    },
    inject: [ConfigService],
  },
];
