import { IsNotEmpty, IsString, IsEmail } from 'class-validator';

export class LoginRequest {
  @IsNotEmpty()
  @IsString()
  @IsEmail()
  username!: string;

  @IsNotEmpty()
  @IsString()
  password!: string;
}
