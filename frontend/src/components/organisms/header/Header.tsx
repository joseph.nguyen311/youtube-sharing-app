/** @jsxImportSource @emotion/react */
import HomeIcon from '@mui/icons-material/Home';
import _get from 'lodash/get';
import { Stack, Typography } from '@mui/material';
import { Link, useNavigate } from 'react-router-dom';
import { FC } from 'react';
import { ILoginRequest, IUserInfo } from 'types/auth';
import LoginForm from 'components/molecules/loginForm/LoginForm';
import { Button } from 'components/atoms';
import { ROUTE_PATHS } from 'routes';
import { useAppDispatch } from 'store';
import { runGetUserInfo, runLogin, userLogout } from 'store/reducers/auth';
import { showToast } from 'store/reducers/common';
import styles from './Header.styles';

interface IProps {
	isLogin: boolean;
	userInfo?: IUserInfo
}

const Header: FC<IProps> = ({ userInfo, isLogin }) => {
	const dispath = useAppDispatch();
	const navigate = useNavigate();

	const handleLogin = async (params: ILoginRequest) => {
		const { payload } = await dispath(runLogin(params));

		const accessToken = _get(payload, 'response.access_token');

		if (!!accessToken) {
			dispath(runGetUserInfo({}));
		} else {
			dispath(showToast({
				title: 'Error',
				message: _get(payload, 'response.access_token'),
			}));
		}
	};

	const handleLogout = () => {
		dispath(userLogout());
	};

	const navigateToShareVideoPage = () => {
		navigate(ROUTE_PATHS.SHARE_VIDEO);
	};

	const renderUserInfo = () => {
		if (isLogin) {
			return (
				<Stack direction={{ sm: 'column', md: 'row' }} spacing={2} sx={{ alignItems: 'center' }}>
					<div>Welcome {userInfo?.username}</div>
					<div><Button variant='contained' onClick={navigateToShareVideoPage}>Share a movie</Button></div>
					<div><Button onClick={handleLogout}>Logout</Button></div>
				</Stack>

			);
		}

		return <LoginForm onSubmit={handleLogin} />
	};

	return (
		<div css={styles.wrapper}>
			<div css={styles.leftContent}>
				<Link css={styles.row} to='/'>
					<HomeIcon color='primary' fontSize='large' />
					<Typography variant='h4' component='p'>Funy Video</Typography>
				</Link>
			</div>
			{renderUserInfo()}
		</div>
	);
};

export default Header;
