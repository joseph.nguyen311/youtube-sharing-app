import { Module } from '@nestjs/common';
import { UsersService, PasswordService } from './services';
import { DatabaseModule } from 'src/database/database.module';
import { userProviders } from './providers';

@Module({
  imports: [DatabaseModule],
  providers: [...userProviders, UsersService, PasswordService],
  exports: [UsersService, PasswordService],
})
export class UsersModule {}
