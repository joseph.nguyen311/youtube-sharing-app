export class UserDto {
  sub: number;
  username: string;
  iat: number;
  exp: number;
}
