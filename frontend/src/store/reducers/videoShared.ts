import { createSlice } from '@reduxjs/toolkit';
import { getListVideoShared, shareVideo } from 'api/videoShare';
import { createCallApiAsyncAction } from 'store/middlewares/createAsyncAction';
import { IVideoItem } from 'types/videoShare';
export const TYPE = 'VIDEO_SHARE';

export const handleGetlistVideoShared = createCallApiAsyncAction(TYPE + '/share', getListVideoShared);
export const handleShareVideo = createCallApiAsyncAction(TYPE + '/share', shareVideo);

interface IVideoSharedState {
  listVideoShared: IVideoItem[];
  isLoading?: boolean;
  error?: string;
}

const initialState: IVideoSharedState = {
  listVideoShared: [],
  isLoading: false,
  error: '',
};

const videoSharedSlice = createSlice({
  name: TYPE,
  initialState,
  reducers: {},
  extraReducers(builder) {
    builder.addCase(handleGetlistVideoShared.fulfilled, (state, action) => {
      const { response } = action.payload

      if (response.apiStatus) {
        state.listVideoShared = response?.data
      }
    })
  },
});

export default videoSharedSlice.reducer;
