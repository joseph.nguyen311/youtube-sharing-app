/* istanbul ignore next */
import { Module } from '@nestjs/common';
import { AppController } from './controllers';
import { AppService } from './services';
import { ConfigModule } from '@nestjs/config';
import { AuthModule } from '../modules/auth';
import { LinkModule } from '../modules/links';

@Module({
  imports: [ConfigModule.forRoot({ isGlobal: true }), AuthModule, LinkModule],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
