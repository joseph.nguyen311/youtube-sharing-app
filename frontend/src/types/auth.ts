export interface ILoginRequest {
    username: string;
    password?: string;
}

export interface ILoginResponse {
    data?: any;
    access_token?: string;
}

export interface IUserInfo {
    username?: string;
    id?: number | string;
}
