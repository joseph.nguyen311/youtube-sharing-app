import { Test, TestingModule } from '@nestjs/testing';
import { AuthController } from './auth.controller';
import { AuthService } from '../services/auth.service';
import { ConfigService } from '@nestjs/config';
import { JwtService } from '@nestjs/jwt';

describe('AuthController', () => {
  let controller: AuthController;
  let authService: AuthService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [AuthController],
      providers: [
        {
          provide: AuthService,
          useValue: {
            signIn: jest.fn(),
          },
        },
        ConfigService,
        JwtService,
      ],
    }).compile();

    controller = module.get<AuthController>(AuthController);
    authService = module.get<AuthService>(AuthService);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
    expect(authService).toBeDefined();
  });

  describe('sign in', () => {
    it('should return what service return', async () => {
      const loginResponse = {
        access_token: 'access_token',
      };
      jest.spyOn(authService, 'signIn').mockResolvedValue(loginResponse);

      const result = await controller.signIn({
        username: 'test@localhost.com',
        password: 'test123',
      });

      expect(result).toEqual(loginResponse);
      expect(authService.signIn).toHaveBeenCalled();
    });
  });
});
