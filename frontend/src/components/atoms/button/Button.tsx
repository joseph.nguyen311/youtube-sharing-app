import MuiButton, { ButtonProps } from '@mui/material/Button';
import { FC } from 'react';

const Button: FC<ButtonProps> = (props) => {
	return (
		<MuiButton sx={{ textTransform: 'capitalize' }} {...props}>
			{props.children}
		</MuiButton>
	);
}

export default Button;
