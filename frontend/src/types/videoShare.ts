export interface IShareVideoRequest {
  url: string;
}

export interface IVideoItem {
  id: number | string;
  user_id: any;
  meta: any;
  created_at?: string;
  url?: string;
  slug?: string;
}

export interface IListVideoResponse {
  data: IVideoItem[];
}

