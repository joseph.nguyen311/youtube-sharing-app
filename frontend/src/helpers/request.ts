import BaseAxios, { AxiosInstance } from 'axios';
import get from 'lodash/get';
import { store } from 'store';

export type IRequestResponse<ResponseObj> = {
  apiStatus: 0 | 1;
  errorStatus?: any;
  message?: any;
  data?: any;
} & ResponseObj extends infer U
  ? { [K in keyof U]: U[K] }
  : never;

class RequestClass {
  axios: AxiosInstance;
  constructor() {
    this.axios = BaseAxios.create({ timeout: 60000 });
  }
  async call<ResponseObj>(config: {
    url: string;
    method: string;
    data?: any;
    serverBaseUrl?: string;
    headers?: any;
  }) {
    try {
      const serverBaseUrl = process.env.REACT_APP_BASE_API;
      const state = store.getState();
      const token = get(state, 'auth.token');

      if (!config.headers) {
        config = {
          ...config,
          headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
          },
        };
      }
      if (token) {
        config = {
          ...config,
          headers: {
            ...config.headers,
            Authorization: `Bearer ${token}`,
          },
        };
        if (config?.method === 'GET') {
          config.data = undefined;
        }
      }

      config.data = {
        ...config.data,
      };

      const res = await this.axios.request({
        baseURL: config?.serverBaseUrl || serverBaseUrl,
        ...config,
      });

      if (res?.data?.length) {
        return { data: res.data, apiStatus: 1 } as unknown as IRequestResponse<ResponseObj>;
      }

      return { ...res.data, apiStatus: 1 } as IRequestResponse<ResponseObj>

    } catch (error) {
      const errorStatus = get(error, 'response.status', null);
      const data = get(error, 'response.data', {}) || {};

      return {
        ...data,
        apiStatus: 0,
        errorStatus,
      } as unknown as IRequestResponse<ResponseObj>;
    }
  }
}

const Request = new RequestClass();

export { Request };
