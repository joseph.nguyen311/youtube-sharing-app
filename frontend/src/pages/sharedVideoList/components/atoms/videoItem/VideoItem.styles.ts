import { css } from '@emotion/react'
import { baseColors, tabletWidth } from 'theme'
const styles = {
  wrapper: css`
    padding: 16px;
    display: flex;
    gap: 16px;
    margin-bottom: 12px;

    @media only screen and (max-width: ${tabletWidth}px) {
      flex-direction: column;
    }
  `,
  leftContent: css`
    display: flex;

    @media only screen and (max-width: ${tabletWidth}px) {
      justify-content: center;
    }

  `,
  statistics: css`
    display: flex;
    align-items: center;

    label {
      margin-right: 8px;
    }
  `,
  rightContent: css`
    flex: 1;
    display: flex;
    flex-direction: column;
    width: 100%;
    gap: 10px;
    max-height: 225px;
    overflow: hidden;
  `,
  title: css`
    color: ${baseColors.dangerColor};
    font-size: 20px;
  `,
  item: css`
    margin
  `,
}

export default styles
