/* istanbul ignore file */
export default {
  type: 'mysql',
  host: process.env['VSA_MYSQL_HOST'] ?? '127.0.0.1',
  port: process.env['VSA_MYSQL_PORT'] ?? '3306',
  username: process.env['VSA_MYSQL_USER'] ?? 'mysql',
  password: process.env['VSA_MYSQL_PASSWORD'] ?? 'changeme',
  database: process.env['VSA_MYSQL_DB'] ?? 'test_db',
  entities: ['src/modules/**/*.entity{.ts,.js}'],
  migrations: ['src/database/migrations/*{.ts,.js}'],
  cli: {
    migrationsDir: 'src/database/migrations',
  },
  synchronize: false,
};
