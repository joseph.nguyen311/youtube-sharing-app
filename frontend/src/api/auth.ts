import { Request } from 'helpers/request';
import { ILoginRequest, ILoginResponse, IUserInfo } from 'types/auth';
const serverBaseUrl = process.env.REACT_APP_BASE_AUTH_API;

export const login = (payload: ILoginRequest) => {
  return Request.call<ILoginResponse>({
    url: '/auth/login',
    serverBaseUrl: serverBaseUrl,
    method: 'POST',
    data: {
      ...payload,
    },
  });
};

export const getUserInfo = () => {
  return Request.call<IUserInfo>({
    url: '/auth/profile',
    serverBaseUrl: serverBaseUrl,
    method: 'GET',
  })
}
