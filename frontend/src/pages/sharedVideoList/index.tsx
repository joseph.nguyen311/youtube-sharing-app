/** @jsxImportSource @emotion/react */
import { Container } from '@mui/material';
import { useEffect } from 'react';
import _isArray from 'lodash/isArray';
import { useAppDispatch, useAppSelector } from 'store';
import { handleGetlistVideoShared } from 'store/reducers/videoShared';
import styles from './ShareVideoList.styles';
import VideoItem from './components/atoms/videoItem';

const SharedVideoList = () => {
	const dispatch = useAppDispatch();
	const videoList = useAppSelector((state) => state?.videoShared?.listVideoShared);

	useEffect(() => {
		dispatch(handleGetlistVideoShared({}));
	}, []);

	if (_isArray(videoList) && videoList.length < 1) {
		return <div css={styles.emptyContent}>No Video</div>
	}

	return (
		<Container maxWidth='md'>
			{_isArray(videoList) && videoList.map((v) => (
				<VideoItem key={v?.id} video={v} />
			))}
		</Container>
	);
};

export default SharedVideoList;
