import { Inject, Injectable } from '@nestjs/common';
import { USER_REPOSITORY } from 'src/common';
import { Repository } from 'typeorm';
import { UserEntity } from '../entities';
import { SignUpDto } from '../dtos';
import { PasswordService } from './password.service';

@Injectable()
export class UsersService {
  constructor(
    @Inject(USER_REPOSITORY)
    private userRepository: Repository<UserEntity>,
    private passwordService: PasswordService,
  ) {}

  async findOne(username: string): Promise<UserEntity | null> {
    return this.userRepository.findOne({
      where: {
        username,
      },
    });
  }

  async create(signupDto: SignUpDto): Promise<UserEntity> {
    const hashPassword = await this.passwordService.toHash(signupDto.password);
    const item = new UserEntity();
    item.username = signupDto.username.replace(/\s+/g, '');
    item.password = hashPassword;
    return await this.userRepository.save(item);
  }
}
