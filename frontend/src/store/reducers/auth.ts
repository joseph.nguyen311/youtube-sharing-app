import { createSlice } from '@reduxjs/toolkit';
import type { PayloadAction } from '@reduxjs/toolkit';
import { login, getUserInfo } from 'api/auth';
import { createCallApiAsyncAction } from 'store/middlewares/createAsyncAction';
import { IUserInfo } from 'types/auth';
export const TYPE = 'AUTH';

export const runLogin = createCallApiAsyncAction(TYPE + '/auth/login', login);
export const runGetUserInfo = createCallApiAsyncAction(TYPE + '/auth/profile', getUserInfo);

interface IAuthState {
  userInfo: IUserInfo;
  token?: string;
  isLoading?: boolean;
  error?: string;
}

const initialState: IAuthState = {
  userInfo: {},
  token: '',
  isLoading: false,
  error: '',
};

const authSlice = createSlice({
  name: TYPE,
  initialState,
  reducers: {
    setUserToken(state, action: PayloadAction<string>) {
      state.isLoading = false;
      state.error = '';
      state.token = action.payload;
    },
    userLogout(state) {
      state.isLoading = false;
      state.error = '';
      state.token = '';
      state.userInfo = {};
    },
  },
  extraReducers(builder) {
    builder.addCase(runGetUserInfo.fulfilled, (state, action) => {
      const { response } = action.payload

      if (response.apiStatus) {
        state.userInfo = response
      }
    })
    builder.addCase(runLogin.fulfilled, (state, action) => {
      const { response } = action.payload

      if (response.apiStatus) {
        state.token = response?.access_token
      }
    })
  },
});

export const { setUserToken, userLogout } = authSlice.actions;
export default authSlice.reducer;
