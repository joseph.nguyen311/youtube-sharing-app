import { css } from '@emotion/react'
import { tabletWidth } from 'theme'
const styles = {
  wrapper: css`
    padding: 16px;
    display: flex;
    gap: 16px;

    @media only screen and (max-width: ${tabletWidth}px) {
      flex-direction: column;

      label {
        margin: 0
      }
    }
  `,
  leftContent: css`
    display: flex;

    label {
      margin-top: 12px;
    }
  `,
  row: css`
    display: flex;
    text-decoration: none;
  `,
  rightContent: css`
    flex: 1;
    display: flex;
    flex-direction: column;
    width: 100%;
  `,
  emptyContent: css`
    text-align: center;
    margin-top: 10%;
    font-size: 20px;
  `,
}

export default styles
