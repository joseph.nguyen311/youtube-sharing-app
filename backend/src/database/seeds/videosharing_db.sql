-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Oct 29, 2023 at 01:26 PM
-- Server version: 5.7.43
-- PHP Version: 7.4.33

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `videosharing_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `links`
--

CREATE TABLE `links` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `slug` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `url` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `meta` longtext COLLATE utf8mb4_unicode_ci,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `links`
--

INSERT INTO `links` (`id`, `user_id`, `slug`, `url`, `meta`, `created_at`) VALUES
(5, 1, '7NgCtqMsklc', 'https://www.youtube.com/watch?v=7NgCtqMsklc', '[{\"id\":\"7NgCtqMsklc\",\"snippet\":{\"publishedAt\":\"2023-10-28T17:15:37Z\",\"channelId\":\"UCT6xyX7Ru_OADyI6N0tqmxA\",\"title\":\"Highlights Al Feiha vs Al Nassr | Ronaldo toả sáng với kiến tạo  - Siêu phẩm xé lưới, mãn nhãn 4 bàn\",\"description\":\"👉Xem ĐỘC QUYỀN Ronaldo và các idol hàng đầu thế giới tại Saudi Pro League trên siêu ứng dụng giải trí VieON. Tải app VieON ngay: https://click.vieon.vn/k9pz/SPLxVNW\\n👉Xem Full các trận đấu hấp dẫn tại: https://www.youtube.com/watch?v=8dy9rA6kXus&list=PLmCIVrTrUS3bbuzdxwDUh3PRY4kBvLf-I\\n#tvhvlog #highlights #ronaldo\",\"thumbnails\":{\"default\":{\"url\":\"https://i.ytimg.com/vi/7NgCtqMsklc/default.jpg\",\"width\":120,\"height\":90},\"medium\":{\"url\":\"https://i.ytimg.com/vi/7NgCtqMsklc/mqdefault.jpg\",\"width\":320,\"height\":180},\"high\":{\"url\":\"https://i.ytimg.com/vi/7NgCtqMsklc/hqdefault.jpg\",\"width\":480,\"height\":360},\"standard\":{\"url\":\"https://i.ytimg.com/vi/7NgCtqMsklc/sddefault.jpg\",\"width\":640,\"height\":480},\"maxres\":{\"url\":\"https://i.ytimg.com/vi/7NgCtqMsklc/maxresdefault.jpg\",\"width\":1280,\"height\":720}},\"channelTitle\":\"TVH Vlogs\",\"tags\":[\"TVH Vlogs\",\"TVH vlog\",\"tuyền văn hóa vlogs\",\"tuyền văn hóa\",\"tuyền văn hóa tv\",\"tuyền văn hóa bình luận bóng đá\",\"tuyền văn hóa messi\",\"tuyền văn hóa ronaldo\",\"bóng đá\",\"nhận định bóng đá\",\"trực tiếp bóng đá\",\"tin bóng đá\",\"bình luận bóng đá\",\"khoảnh khắc bóng đá\",\"highlights bóng đá\",\"tin tức bóng đá\",\"highlights\",\"saudi pro league\",\"giải vô địch quốc gia ả rập xê út\",\"tvh\"],\"categoryId\":\"17\",\"liveBroadcastContent\":\"none\",\"localized\":{\"title\":\"Highlights Al Feiha vs Al Nassr | Ronaldo toả sáng với kiến tạo  - Siêu phẩm xé lưới, mãn nhãn 4 bàn\",\"description\":\"👉Xem ĐỘC QUYỀN Ronaldo và các idol hàng đầu thế giới tại Saudi Pro League trên siêu ứng dụng giải trí VieON. Tải app VieON ngay: https://click.vieon.vn/k9pz/SPLxVNW\\n👉Xem Full các trận đấu hấp dẫn tại: https://www.youtube.com/watch?v=8dy9rA6kXus&list=PLmCIVrTrUS3bbuzdxwDUh3PRY4kBvLf-I\\n#tvhvlog #highlights #ronaldo\"}},\"statistics\":{\"viewCount\":\"210351\",\"likeCount\":\"1855\",\"favoriteCount\":\"0\",\"commentCount\":\"221\"}}]', '2023-10-29 20:25:40');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(11) NOT NULL,
  `timestamp` bigint(20) NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `username` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(500) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `username`, `password`, `created_at`) VALUES
(1, 'joseph.nguyen311@gmail.com', '2782075150406184db8b580ac97ecd8dea970ccbaf4264565125eb6949442d3aee3c3c680bf4092735557ecda437358bed067b7e8430a6a37cbe7c8c23d6d307.57625977f8a9e94e', '2023-10-28 23:45:59'),
(2, 'guess01@test.com', '2166a6cdbdb9bfac7a758199f48bd4e72bcbb9702596843be69d3acb90be1288ab078d1c7499342e13c1404cef7b66294bf748ad489724db8ab815dafa642e48.df638e17662615f9', '2023-10-28 23:45:59'),
(3, 'joseph.nguyen312@gmail.com', '2782075150406184db8b580ac97ecd8dea970ccbaf4264565125eb6949442d3aee3c3c680bf4092735557ecda437358bed067b7e8430a6a37cbe7c8c23d6d307.57625977f8a9e94e', '2023-10-29 00:35:52');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `links`
--
ALTER TABLE `links`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `links`
--
ALTER TABLE `links`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
