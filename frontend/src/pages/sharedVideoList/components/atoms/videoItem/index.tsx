
/** @jsxImportSource @emotion/react */
import { Container, Typography } from '@mui/material';
import ThumbUpAltOutlinedIcon from '@mui/icons-material/ThumbUpAltOutlined';
import ThumbDownAltOutlinedIcon from '@mui/icons-material/ThumbDownAltOutlined';
import _get from 'lodash/get';
import { FC } from 'react';
import { IVideoItem } from 'types/videoShare';
import styles from './VideoItem.styles';

interface IProps {
  video: IVideoItem
}

const VideoItem: FC<IProps> = ({ video }) => {
  return (
    <Container key={`v-${video?.id}`} maxWidth='lg'>
      <div css={styles.wrapper}>
        <div css={styles.leftContent}>
          <iframe
            title={`iframe=${video?.id}`}
            allowFullScreen
            width='100%'
            height='225'
            src={`https://www.youtube.com/embed/${video?.slug}`}>
          </iframe>
        </div>
        <div css={styles.rightContent}>
          <Typography css={[styles.title]}>{_get(video, 'meta.videos.[0].snippet.title')}</Typography>
          <div>Shared by: {_get(video, 'user_id')}</div>
          <div css={styles.statistics}>
            <label>{_get(video, 'meta.videos.[0].statistics.likeCount')}</label> <ThumbUpAltOutlinedIcon fontSize='small' />
            <label>{_get(video, 'meta.videos.[0].statistics.dislikeCount')}</label> <ThumbDownAltOutlinedIcon fontSize='small' />
          </div>
          <div>Description</div>
          <div>{_get(video, 'meta.videos.[0].snippet.description')}</div>
        </div>
      </div>
    </Container>
  );
};

export default VideoItem;
