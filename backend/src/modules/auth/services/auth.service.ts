import { Injectable, UnauthorizedException } from '@nestjs/common';
import { UsersService, PasswordService } from 'src/modules/users/services';
import { JwtService } from '@nestjs/jwt';

@Injectable()
export class AuthService {
  constructor(
    private usersService: UsersService,
    private jwtService: JwtService,
    private passwordService: PasswordService,
  ) {}

  async signIn(username, pass) {
    let user = await this.usersService.findOne(username);
    if (!user) {
      user = await this.usersService.create({ username, password: pass });
    }

    console.log('user', user);
    if (!this.passwordService.compare(pass, user?.password)) {
      throw new UnauthorizedException();
    }

    const payload = { sub: user.id, username: user.username };
    return {
      access_token: await this.jwtService.signAsync(payload),
    };
  }
}
