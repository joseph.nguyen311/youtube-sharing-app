/* istanbul ignore file */
import ormConfig from './typeorm.config';

export default {
  ...ormConfig,
  migrationsTableName: 'orm_seeeders',
  migrations: ['src/database/seed/*{.ts,.js}'],
  cli: {
    migrationsDir: 'src/database/seed',
  },
};
