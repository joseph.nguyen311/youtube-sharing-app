import { Container } from '@mui/material';
import { FC } from 'react';

import { Header } from 'components/molecules';
import { useAppSelector } from 'store';

interface IProps {
  children: string | JSX.Element | JSX.Element[]
}

const Layout: FC<IProps> = (props) => {
  const accessToken = useAppSelector((state) => state.auth.token)
  const userInfo = useAppSelector((state) => state.auth.userInfo)

  return (
    <Container maxWidth='lg'>
      <Header isLogin={!!accessToken} userInfo={userInfo} />
      {props.children}
    </Container>
  );
};

export default Layout;
