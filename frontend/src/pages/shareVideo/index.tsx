/** @jsxImportSource @emotion/react */
import { Container } from '@mui/material';
import { useForm, Controller } from 'react-hook-form';
import { yupResolver } from '@hookform/resolvers/yup';
import * as yup from 'yup';
import _get from 'lodash/get';

import { Button, TextField } from 'components/atoms';
import { handleShareVideo } from 'store/reducers/videoShared';
import { useAppDispatch } from 'store';
import { showToast } from 'store/reducers/common';
import styles from './ShareVideo.styles';

const schema = yup.object({
	url: yup.string().url().required()
		.matches(/^(https?\:\/\/)?(www\.youtube\.com|youtu\.be)\/.+$/, 'Please enter youtube URL'),
}).required();

type FormData = yup.InferType<typeof schema>;

const ShareVideo = () => {
	const dispath = useAppDispatch();
	const { control, handleSubmit, formState: { errors }, setValue } = useForm<FormData>({
		resolver: yupResolver(schema),
	});

	const shareVideo = async (data: any) => {
		const { payload } = await dispath(handleShareVideo(data));
		const is_link = _get(payload, 'response.is_link');

		if (!is_link) {
			dispath(showToast({
				title: 'Erorr',
				message: _get(payload, 'response.message') || 'Invalid URL. Please enter url correctly',
				type: 'error',
			}));
		} else {
			setValue('url', '');
			dispath(showToast({
				title: 'Success',
				message: 'Share video successfully',
				type: 'success',
			}));
		}
	};

	return (
		<form onSubmit={handleSubmit(shareVideo)}>
			<Container sx={{ marginTop: '10%' }} maxWidth='sm'>
				<fieldset css={styles.fieldset}>
					<legend>Share a Youtube movie</legend>
					<div css={styles.wrapper}>
						<div css={styles.leftContent}>
							<label>Youtube URL:</label>
						</div>
						<div css={styles.rightContent}>
							<Controller
								name='url'
								control={control}
								render={({ field }) => <TextField helperText={errors?.url?.message} error={!!errors?.url?.message} label='Youtube URL' {...field} />}
							/>
							<Button sx={{ width: '100%', marginTop: '20px' }} type='submit' variant='contained'>Share</Button>
						</div>
					</div>
				</fieldset>
			</Container>

		</form>
	);
};

export default ShareVideo;
