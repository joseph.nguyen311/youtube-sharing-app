import { Module } from '@nestjs/common';
import { JwtModule } from '@nestjs/jwt';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { UsersModule } from 'src/modules/users';
import { LinkService } from './services';
import { LinkController } from './controllers';
import { linkProviders } from './providers';
import { DatabaseModule } from 'src/database/database.module';

@Module({
  imports: [
    DatabaseModule,
    UsersModule,
    JwtModule.registerAsync({
      imports: [ConfigModule],
      inject: [ConfigService],
      useFactory: (configService: ConfigService) => {
        return {
          global: true,
          secret: configService.get<string>('JWT_SECRET_KEY'),
          signOptions: {
            expiresIn: configService.get<string>('JWT_EXPIRED_TIME'),
          },
        };
      },
    }),
  ],
  providers: [...linkProviders, LinkService],
  controllers: [LinkController],
  exports: [LinkService],
})
export class LinkModule {}
