import { Inject, Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { Repository } from 'typeorm';
import axios from 'axios';
import { LINK_REPOSITORY, REQUEST_URL_API, PART } from 'src/common';
import { LinkEntity } from '../entities';
import { YoutubeDataDto } from '../dtos';

@Injectable()
export class LinkService {
  constructor(
    @Inject(LINK_REPOSITORY)
    private linkRepository: Repository<LinkEntity>,
    private configService: ConfigService,
  ) {}

  async checker(url: string, userId: number) {
    const slugLink = await this.matchYoutubeUrl(url);
    let link = null;
    if (slugLink) {
      const linkCur = await this.findOne(slugLink);
      link = linkCur;
      if (!linkCur) {
        const apiKey = this.configService.get('VSA_YOUTUBE_API_KEY');
        const fullLink = `${REQUEST_URL_API}${slugLink}&key=${apiKey}${PART}`;
        const data = await this.getYouTubeData(fullLink);

        const item = new LinkEntity();
        item.slug = slugLink;
        item.user_id = userId;
        item.url = url;
        item.meta = !data ? null : data;
        this.linkRepository.save(item);
        link = item;
      }
    }

    return {
      is_link: !!slugLink,
      link,
    };
  }

  async getLinks(userId: string) {
    return [{ userId }];
  }

  private async matchYoutubeUrl(url: string) {
    const p =
      /^(?:https?:\/\/)?(?:www\.)?(?:youtu\.be\/|youtube\.com\/(?:embed\/|v\/|watch\?v=|watch\?.+&v=))((\w|-){11})(?:\S+)?$/;
    const matches = url.match(p);
    if (matches) {
      return matches[1];
    }
    return false;
  }

  async findOne(slug: string): Promise<LinkEntity | null> {
    return this.linkRepository.findOne({
      where: {
        slug,
      },
    });
  }

  private async getYouTubeData(link: string) {
    try {
      const { data } = await axios.get<YoutubeDataDto>(link);
      return data.items;
    } catch (error) {
      if (axios.isAxiosError(error)) {
        console.log('error message: ', error.message);
      } else {
        console.log('unexpected error: ', error);
      }
      return null;
    }
  }
}
