import { Test, TestingModule } from '@nestjs/testing';
import { JwtService } from '@nestjs/jwt';
import { AuthService } from './auth.service';
import { UsersService, PasswordService } from 'src/modules/users/services';
import { USER_REPOSITORY, LINK_REPOSITORY } from 'src/common';

describe('AuthService', () => {
  let service: AuthService;
  let jwtService: JwtService;
  let passService: PasswordService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        AuthService,
        UsersService,
        PasswordService,
        {
          provide: USER_REPOSITORY,
          useValue: USER_REPOSITORY,
        },
        {
          provide: LINK_REPOSITORY,
          useValue: LINK_REPOSITORY,
        },
        {
          provide: JwtService,
          useValue: {
            signAsync: jest.fn(),
            verifyAsync: jest.fn(),
          },
        },
      ],
    }).compile();

    service = module.get<AuthService>(AuthService);
    jwtService = module.get<JwtService>(JwtService);
    passService = module.get<PasswordService>(PasswordService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
    expect(jwtService).toBeDefined();
    expect(passService).toBeDefined();
  });
});
