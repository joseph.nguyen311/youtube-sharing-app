import React from 'react';
import MuiTextField, { TextFieldProps } from '@mui/material/TextField';

export type Ref = HTMLButtonElement;

const TextField = React.forwardRef<Ref, TextFieldProps>(
  (
    props,
    ref: React.ForwardedRef<Ref>,
  ): JSX.Element => {
    return (
      <MuiTextField size='small' {...props}>
        {props.children}
      </MuiTextField>
    );
  },
);

export default TextField;
