import { createTheme } from '@mui/material/styles'
// import { css } from '@emotion/react'

export const baseColors = {
  primaryLinearGradient: 'linear-gradient(180deg, #FFBAA1 0%, #FD3F46 100%)',
  primaryColor: '#F89562',
  primaryColor3: '#FEA090',
  primaryColor4: '#FD7D74',

  secondaryColor: '#ABC1EB',
  secondaryColor2: '#D0E0F7',
  secondaryColor3: '#F5F5F5',
  secondaryColor5: '#6D83B4',

  neutralColor1: '#FFFFFF',
  neutralColor2: '#FAFAFA',
  neutralColor6: '#BFBFBF',
  neutralColor7: '#8C8C8C',
  neutralColor12: '#141414',

  darkSecondary30: '#5A5B74',
  darkSecondary80: '#8A8CB2',

  darkLink80: '#9FBBFF',

  darkGreen70: '#9BFFBC',
  darkGray70: '#CCC',

  appBg: '#171828',
  cardBg: '#121426',
  imgBg: '#1B2030',
  toastBg: '#323255',

  borderColor: 'rgba(0, 0, 0, 0.23)',

  filterLabel: '#c4c5d9',
  darkPrimary: '#f45c0e',

  progressBarBackground: '#5A5B74',
  progressBarColor1: '#9FBCFF',
  progressBarColor2: '#9BFEBC',
  progressBarColor3: '#FCEE66',
  linkText: '#3ed9fb',

  blur: '#0002',
  white: '#fff',
  black: '#000',

  tagBackground: '#2B3755',

  successColor4: '#56E787',
  warningColor2: '#FEFB9D',
  dangerColor: '#F87C73',

  lifetime: '#12FFE6',

  bgTabBar: '#1B2030',
  switchButton: '#FF1F3A',
  dropdownBg: '#242634',
}

export const globalTheme = createTheme({
  components: {
    MuiTypography: {
      defaultProps: {
        color: baseColors.neutralColor12,
      },
    },
  },
})

export const tabletWidth = 1023
export const mobileWidth = 768

