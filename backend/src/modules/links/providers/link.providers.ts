import { DataSource } from 'typeorm';
import { LinkEntity } from '../entities';
import { DATA_SOURCE, LINK_REPOSITORY } from 'src/common';

export const linkProviders = [
  {
    provide: LINK_REPOSITORY,
    useFactory: (dataSource: DataSource) =>
      dataSource.getRepository(LinkEntity),
    inject: [DATA_SOURCE],
  },
];
