/** @jsxImportSource @emotion/react */
import { ThemeProvider } from '@mui/material/styles'
import { CacheProvider } from '@emotion/react'

import createCache from '@emotion/cache'
import { BrowserRouter } from 'react-router-dom'

import { globalTheme } from 'theme'
import Router from 'routes'
import GlobalAlert from 'components/atoms/globalAlert/GlobalAlert'
import { Layout } from 'components/templates'

const cache = createCache({
  key: 'css',
  prepend: true,
})

const App = () => {
  return (
    <CacheProvider value={cache}>
      <ThemeProvider theme={globalTheme}>
        <BrowserRouter>
          <Layout>
            <Router />
            <GlobalAlert />
          </Layout>
        </BrowserRouter>
      </ThemeProvider>
    </CacheProvider>
  )
}

export default App
