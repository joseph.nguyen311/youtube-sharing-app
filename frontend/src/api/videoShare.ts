import { Request } from 'helpers/request';
import { IListVideoResponse, IVideoItem, IShareVideoRequest } from 'types/videoShare';
const serverBaseUrl = process.env.REACT_APP_BASE_AUTH_API;

export const shareVideo = (payload: IShareVideoRequest) => {
  return Request.call<IVideoItem>({
    url: '/link/checker',
    serverBaseUrl: serverBaseUrl,
    method: 'POST',
    data: {
      ...payload,
    },
  });
};

export const getListVideoShared = () => {
  return Request.call<IListVideoResponse>({
    url: '/link',
    serverBaseUrl: serverBaseUrl,
    method: 'GET',
  })
}
