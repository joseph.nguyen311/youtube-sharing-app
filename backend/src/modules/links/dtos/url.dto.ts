import { IsNotEmpty, IsString, IsUrl } from 'class-validator';

export class UrlRequestDto {
  @IsNotEmpty()
  @IsString()
  @IsUrl()
  url!: string;
}
