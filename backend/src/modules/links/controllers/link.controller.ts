import {
  UsePipes,
  ValidationPipe,
  Body,
  Controller,
  Post,
  UseGuards,
  Get,
  Req,
} from '@nestjs/common';
import { AuthGuard } from 'src/modules/auth/guard';
import { LinkService } from '../services/link.service';
import { UrlRequestDto } from '../dtos';
import { UserDto } from 'src/modules/users/dtos';

@Controller('link')
export class LinkController {
  constructor(private linkService: LinkService) {}

  @UseGuards(AuthGuard)
  @Post('checker')
  @UsePipes(new ValidationPipe())
  checker(@Body() urlRequestDto: UrlRequestDto, @Req() req: any) {
    const user = <UserDto>req.user;
    return this.linkService.checker(urlRequestDto.url, user.sub);
  }

  @UseGuards(AuthGuard)
  @Get('')
  getLinks(@Req() req) {
    const userId = req.user?.sub;
    return this.linkService.getLinks(userId);
  }
}
