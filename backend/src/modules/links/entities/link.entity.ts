import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';

@Entity('links')
export class LinkEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  user_id!: number;

  @Column({ length: 50 })
  slug: string;

  @Column({ length: 255 })
  url!: string;

  @Column({ type: 'json' })
  meta: string;

  @Column({ type: 'timestamp', default: () => 'CURRENT_TIMESTAMP' })
  created_at: Date;
}
